#! /usr/bin/env python

import os
import sys
import random
import pygame

APP_NAME = "YATC"

class Color(object):
    @staticmethod
    def WHITE():
        return Color([255, 255, 255])
    
    @staticmethod
    def GRAY():
        return Color([128, 128, 128])
    
    @staticmethod
    def BLACK():
        return Color([0, 0, 0])
    
    @staticmethod
    def CYAN():
        return Color([0, 255, 255])
    
    @staticmethod
    def BLUE():
        return Color([0, 0, 255])
    
    @staticmethod
    def ORANGE():
        return Color([255, 127, 0])
    
    @staticmethod
    def YELLOW():
        return Color([255, 255, 0])
    
    @staticmethod
    def GREEN():
        return Color([0, 255, 0])
    
    @staticmethod
    def PURPLE():
        return Color([128, 0, 128])
    
    @staticmethod
    def RED():
        return Color([255, 0, 0])
    
    def __init__(self, (r, g, b)):
        self.r = r
        self.g = g
        self.b = b
        self.rgb = [self.r, self.g, self.b]
    
    def invert(self):
        return Color([255 - self.r, 255 - self.g, 255 - self.b])


class Well(object):
    EMPTY = -2
    SOLID = -1
    SPAWN_SPACE_H = 4
    (W, H) = (10, 20)
    
    def __init__(self, (w, h)):
        self.w = w
        self.h_actual = h
        self.h = h + Well.SPAWN_SPACE_H
        self.matrix = [[Well.EMPTY] * self.h for i in range(self.w)]
    
    def paint(self, surface, scale, color_map, (xo, yo)=(0, 0)):
        for x in range(self.w):
            for y in range(self.h):
                type = self.matrix[x][y]
                if type == Well.EMPTY:
                    continue
                x_actual = xo + x * scale
                y_actual = yo + (y - Well.SPAWN_SPACE_H) * scale
                color = color_map[type]
                if Ui.FANCY_COL:
                    square = [x_actual, y_actual, scale * 4 / 5, scale * 4 / 5]
                    draw_rect(surface, color, square)
                    draw_rect(surface, color.invert(), square, scale / 5)
                else:
                    square = [x_actual, y_actual, scale, scale]
                    draw_rect(surface, color, square)
    
    def delete_line(self, n):
        for y in range(n, 0, -1):
            for x in range(self.w):
                self.matrix[x][y] = self.matrix[x][y - 1]
    
    def clear_lines(self):
        rows = zip(*self.matrix)
        full = [Well.SOLID] * self.w
        cleared = 0
        for i in range(self.h):
            if list(rows[i]) == full:
                self.delete_line(i)
                cleared = cleared + 1
        return cleared


class Tetrimino(object):
    I, L, J, O, S, T, Z = range(7)
    
    @staticmethod
    def types():
        return {Tetrimino.I:([(0, 0), (-1, 0), (-2, 0), (1, 0)], 2),
                Tetrimino.L:([(0, 0), (-1, 0), (-1,-1), (1, 0)], 4),
                Tetrimino.J:([(0, 0), (-1, 0), ( 1, 0), (1,-1)], 4),
                Tetrimino.O:([(0, 0), ( 1, 0), ( 0, 1), (1, 1)], 1),
                Tetrimino.S:([(0, 0), ( 1, 0), ( 0, 1), (-1,1)], 2),
                Tetrimino.T:([(0, 0), (-1, 0), ( 1, 0), (0,-1)], 4),
                Tetrimino.Z:([(0, 0), (-1, 0), ( 0, 1), (1, 1)], 2)}
    
    @staticmethod
    def random_type():
        return random.randint(0, len(Tetrimino.types()) - 1)
    
    @staticmethod
    def paint(surface, (xo, yo), type, scale, color_map):
        blocks = Tetrimino.types()[type][0]
        for (x, y) in blocks:
            x_actual = xo + x * scale
            y_actual = yo + y * scale
            color = color_map[type]
            if Ui.FANCY_COL:
                square = [x_actual, y_actual, scale * 4 / 5, scale * 4 / 5]
                draw_rect(surface, color, square)
                draw_rect(surface, color.invert(), square, scale / 5)
            else:
                square = [x_actual, y_actual, scale, scale]
                draw_rect(surface, color, square)
    
    def __init__(self, well, pos=None, type=None):
        if pos is None and well is not None:
            pos = (well.w / 2, 2)
        if type is None:
            type = Tetrimino.random_type()
        self.well = well
        self.x, self.y = pos
        self.type = type
        self.blocks, self.max_rotate = Tetrimino.types()[self.type]
        self.cur_rotate = 0
        if self.well is not None:
            if self.fits():
                self.insert_in_well()
    
    def absolute(self, dx=0, dy=0, blocks=None):
        if blocks is None:
            blocks = self.blocks
        return [(x + dx + self.x, y + dy + self.y) for (x, y) in blocks]
    
    def fits(self, new_absolute=None):
        if new_absolute is None:
            new_absolute = self.absolute()
        for (x, y) in new_absolute:
            if y >= self.well.h or y < 0:
                return False
            if x >= self.well.w or x < 0:
                return False
            if self.well.matrix[x][y] == Well.SOLID:
                return False
        return True
    
    def move(self, (dx, dy)):
        moved_absolute = self.absolute(dx=dx, dy=dy)
        if self.fits(moved_absolute):
            self.insert_in_well(new=moved_absolute)
            self.x = self.x + dx
            self.y = self.y + dy
            return True
        return False
    
    def left(self):
        self.move((-1, 0))
        return (self, 0)
    
    def right(self):
        self.move((1, 0))
        return (self, 0)
    
    def down(self):
        if self.move((0, 1)):
            return (self, 0)
        else:
            return (None, self.land())
    
    def rotate(self):
        if self.max_rotate > 1:
            self.cur_rotate = self.cur_rotate + 1
            if self.cur_rotate == 2 and self.max_rotate == 2:
                self.cur_rotate = self.cur_rotate % self.max_rotate
                return self.rotate_anticlockwise()
            else:
                self.cur_rotate = self.cur_rotate % self.max_rotate
                return self.rotate_clockwise()
        else:
            return self
    
    def rotate_anticlockwise(self):
        self.rotate_clockwise()
        self.rotate_clockwise()
        return self.rotate_clockwise()
    
    def rotate_clockwise(self):
        rotated_blocks = [(-y, x) for (x, y) in self.blocks]
        rotated_abs_blocks = self.absolute(blocks=rotated_blocks)
        if self.fits(rotated_abs_blocks):
            self.insert_in_well(new=rotated_abs_blocks)
            self.blocks = rotated_blocks
        return self
    
    def land(self):
        self.insert_in_well(solid=True)
        return self.well.clear_lines()
    
    def insert_in_well(self, old=None, new=None, solid=False):
        if old is None:
            old = self.absolute()
        if new is None:
            new = self.absolute()
        if solid:
            for (x, y) in old:
                self.well.matrix[x][y] = Well.SOLID
        else:
            for (x, y) in old:
                self.well.matrix[x][y] = Well.EMPTY
                for (x, y) in new:
                    self.well.matrix[x][y] = self.type


class Game(object):
    FPS = 40
    LEVEL_UP = 10
    INIT_LEVEL = 0
    ENABLE_SLIDING = False
    LEFT = pygame.K_LEFT
    RIGHT = pygame.K_RIGHT
    DOWN = pygame.K_DOWN
    ROTATE = pygame.K_UP
    PAUSE = pygame.K_p
    EXIT1 = pygame.K_e
    EXIT2 = pygame.K_ESCAPE
    RESTART = pygame.K_r
    
    def __init__(self, size, scale, surface, colors, is_paused=True):
        self.well = Well(size)
        self.next_piece = Tetrimino.random_type()
        self.piece = self.new_piece()
        self.level = Game.INIT_LEVEL
        self.cleared = 0
        self.score = 0
        self.is_over = False
        self.is_running = True
        self.is_paused = is_paused
        self.init_down_delay = 1000.0
        self.down_delay = self.init_down_delay / (self.level + 1)
        self.time = 0.0
        self.repaint = True
        self.scale = scale
        self.surface = surface
        self.color_map = colors
        self.surface.fill(self.color_map[Ui.BG].rgb)
    
    def get_rect(self):
        (x, y) = (0, 0)
        (w, h) = (self.well.w * self.scale, self.well.h_actual * self.scale)
        return pygame.Rect((x, y), (w, h))
    
    def new_piece(self):
        piece = Tetrimino(well=self.well, type=self.next_piece)
        self.next_piece = Tetrimino.random_type()
        if not piece.fits():
            piece.land()
            self.is_over = True
            return None
        else:
            return piece
    
    def tick(self):
        if not self.is_paused:
            self.time = self.time + Game.FPS
            if self.time >= self.down_delay:
                self.time = 0.0
                self.request_down()
    
    def request_pause(self):
        self.is_paused = not self.is_paused
    
    def request_down(self):
        if self.piece is not None:
            (self.piece, num_cleared) = self.piece.down()
            if self.piece is None:
                self.piece = self.new_piece()
            if num_cleared > 0:
                self.score = self.score + self.score_lines(num_cleared)
                self.cleared = self.cleared + num_cleared
                if self.cleared >= Game.LEVEL_UP * (self.level + 1):
                    self.level = self.level + 1
                    self.down_delay = self.init_down_delay / (self.level + 1)
    
    def request_softdrop(self):
        if not self.is_paused:
            self.request_down()
            self.score = self.score + 1
    
    def request_left(self):
        if not self.is_paused:
            self.piece.left()
    
    def request_right(self):
        if not self.is_paused:
            self.piece.right()
    
    def request_rotate(self):
        if not self.is_paused:
            self.piece.rotate()
    
    def paint(self):
        if self.repaint:
            self.surface.fill(self.color_map[Ui.BG].rgb, self.get_rect())
            self.well.paint(self.surface, self.scale, self.color_map)
    
    def score_lines(self, lines):
        if lines == 1:
            return 30 * (self.level + 1)
        elif lines == 2:
            return 100 * (self.level + 1)
        elif lines == 3:
            return 300 * (self.level + 1)
        elif lines == 4:
            return 1200 * (self.level + 1)


class Ui(object):
    FG = 100
    BG = 101
    (W, H) = (5, 20)
    SCALE = 25
    TEXT_BIG = SCALE * 2
    TEXT_SMALL = SCALE
    FANCY_COL = False
    
    def __init__(self, (win_w, win_h), scale, surface, color_map, game):
        self.w = win_w
        self.h = win_h
        self.scale = scale
        self.game = game
        self.game_rect = game.get_rect()
        self.score_old = game.score
        self.level_old = game.level
        self.cleared_old = game.cleared
        self.nextpiece_old = game.next_piece
        self.ispaused_old = False
        self.isover_old = game.is_over
        self.ui_rect = self.get_rect()
        self.surface = surface
        self.color_map = color_map
        self.paint(repaint_static=True, force_repaint_dynamic=True)
    
    def get_rect(self):
        game_topright = (gtr_x, gtr_y) = self.game_rect.topright
        ui_topleft = (gtr_x + self.scale / 5, gtr_y)
        w = self.w - gtr_x - self.scale / 5
        h = self.h
        return pygame.Rect(ui_topleft, (w, h))
    
    def paint(self, repaint_static=False, force_repaint_dynamic=False):
        fg = self.color_map[Ui.FG]
        bg = self.color_map[Ui.BG]
        (game_x, game_y) = self.game_rect.bottomright
        (ui_x, ui_y) = self.ui_rect.bottomleft
        (ax, ay) = (self.ui_rect.centerx, Ui.TEXT_BIG / 2)
        (bx, by) = (ax, ay + Ui.TEXT_BIG)
        (cx, cy) = (ax, by + Ui.TEXT_BIG)
        (dx, dy) = (ax, cy + Ui.TEXT_BIG * 2)
        (ex, ey) = (ax, dy + Ui.TEXT_SMALL)
        (fx, fy) = (ax, ey + Ui.TEXT_BIG)
        (gx, gy) = (ax, fy + Ui.TEXT_SMALL)
        (hx, hy) = (ax, gy + Ui.TEXT_BIG)
        (ix, iy) = (ax, hy + Ui.TEXT_SMALL)
        if repaint_static:
            self.surface.fill(bg.rgb, self.ui_rect)
            vls = (game_x, 0)
            vle = (game_x, ui_y)
            vrs = (ui_x, 0)
            vre = (ui_x, ui_y)
            hs = (0, game_y)
            he = (game_x, game_y)
            draw_line(self.surface, fg, vls, vle)
            draw_line(self.surface, fg, vrs, vre)
            draw_line(self.surface, fg, hs, he)
            a = APP_NAME
            b = "Next piece:"
            d = "Score:"
            f = "Level:"
            h = "Cleared:"
            blit_string(self.surface, a, (ax, ay), Ui.TEXT_BIG, fg)
            blit_string(self.surface, b, (bx, by), Ui.TEXT_SMALL, fg)
            blit_string(self.surface, d, (dx, dy), Ui.TEXT_SMALL, fg)
            blit_string(self.surface, f, (fx, fy), Ui.TEXT_SMALL, fg)
            blit_string(self.surface, h, (hx, hy), Ui.TEXT_SMALL, fg)
        if self.game.next_piece != self.nextpiece_old or force_repaint_dynamic:
            (top_left_x, top_left_y) = (ui_x + 1, by + Ui.TEXT_SMALL / 2)
            (w, h) = (self.ui_rect.w, dy - cy)
            rect = [(top_left_x, top_left_y), (w, h)]
            self.surface.fill(bg.rgb, rect)
            type = self.game.next_piece
            pos = (cx, cy)
            Tetrimino.paint(self.surface, pos, type, self.scale, self.color_map)
            self.nextpiece_old = self.game.next_piece
        if self.game.score != self.score_old or force_repaint_dynamic:
            e = " %d " % self.game.score
            blit_string(self.surface, e, (ex, ey), Ui.TEXT_SMALL, fg, bg=bg)
            self.score_old = self.game.score
        if self.game.level != self.level_old or force_repaint_dynamic:
            g = " %d " % self.game.level
            blit_string(self.surface, g, (gx, gy), Ui.TEXT_SMALL, fg, bg=bg)
            self.level_old = self.game.level
        if self.game.cleared != self.cleared_old or force_repaint_dynamic:
            i = " %d " % self.game.cleared
            blit_string(self.surface, i, (ix, iy), Ui.TEXT_SMALL, fg, bg=bg)
            self.cleared_old = self.game.cleared
        if self.game.is_paused and not self.ispaused_old:
            a = "%s to start/resume" % chr(Game.PAUSE)
            b = "arrow keys to move & rotate"
            (ax, ay) = (self.game.get_rect().centerx, self.get_rect().centery)
            (bx, by) = (ax, ay + Ui.TEXT_SMALL)
            blit_string(self.surface, a, (ax, ay), Ui.TEXT_SMALL, fg)
            blit_string(self.surface, b, (bx, by), Ui.TEXT_SMALL, fg)
            self.game.repaint = False
            self.ispaused_old = self.game.is_paused
        elif not self.game.is_paused and self.ispaused_old:
            a = "%s to start/resume" % chr(Game.PAUSE)
            b = "arrow keys to move & rotate"
            (ax, ay) = (self.game.get_rect().centerx, self.get_rect().centery)
            (bx, by) = (ax, ay + Ui.TEXT_SMALL)
            blit_string(self.surface, a, (ax, ay), Ui.TEXT_SMALL, fg, unblit=bg)
            blit_string(self.surface, b, (bx, by), Ui.TEXT_SMALL, fg, unblit=bg)
            self.game.repaint = True
            self.ispaused_old = self.game.is_paused
            self.paint(repaint_static=True, force_repaint_dynamic=True)
        if self.game.is_over and not self.isover_old:
            (exit_key, restart_key) = (chr(Game.EXIT1), chr(Game.RESTART))
            a = "Game Over"
            b = "%s to exit or %s to restart" % (exit_key, restart_key)
            (ax, ay) = (self.game.get_rect().centerx, self.get_rect().centery)
            (bx, by) = (ax, ay + Ui.TEXT_SMALL)
            blit_string(self.surface, a, (ax, ay), Ui.TEXT_BIG, fg)
            blit_string(self.surface, b, (bx, by), Ui.TEXT_SMALL, fg)
            self.game.repaint = False
            self.isover_old = self.game.is_over


def blit_string(surface, string, (x, y), size, col, bg=None, unblit=None):
    font = pygame.font.Font(None, size)
    if bg is None:
        text = font.render(string, 1, col.rgb)
    else:
        text = font.render(string, 1, col.rgb, bg.rgb)
    textpos = text.get_rect()
    textpos.centerx = x
    textpos.centery = y
    if unblit is not None:
        surface.fill(unblit.rgb, textpos)
    else:
        surface.blit(text, textpos)
    return textpos

def draw_rect(surface, color, rect, width=0):
    pygame.draw.rect(surface, color.rgb, rect, width)

def draw_line(surface, color, start_pos, end_pos, width=1):
    pygame.draw.line(surface, color.rgb, start_pos, end_pos, width)

def main():
    COLORS = {Well.EMPTY: Color.BLACK(),
              Well.SOLID: Color.GRAY(),
              Ui.FG: Color.WHITE(),
              Ui.BG: Color.BLACK(),
              Tetrimino.I: Color.CYAN(),
              Tetrimino.J: Color.BLUE(),
              Tetrimino.L: Color.ORANGE(),
              Tetrimino.O: Color.YELLOW(),
              Tetrimino.S: Color.GREEN(),
              Tetrimino.T: Color.PURPLE(),
              Tetrimino.Z: Color.RED()}
    WIN_W = Ui.SCALE * (Well.W + Ui.W)
    WIN_H = Ui.SCALE * max(Ui.H, Well.H)
    
    # set up pygame
    os.environ["SDL_VIDEO_CENTERED"] = "1"    
    pygame.init()
    screen = pygame.display.set_mode((WIN_W, WIN_H))
    clock = pygame.time.Clock()
    pygame.display.set_caption(APP_NAME)
    
    # set up yatc
    game = Game((Well.W, Well.H), Ui.SCALE, screen, COLORS)
    ui = Ui((WIN_W, WIN_H), Ui.SCALE, screen, COLORS, game)
    last_keypress = 0
    key_delay = 50
    
    # main loop
    while game.is_running:
        game.paint()
        ui.paint()
        
        if not game.is_over:
            game.tick()
            
            # process user input - ingame
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    game.is_running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == Game.ROTATE:
                        game.request_rotate()
                    elif event.key == Game.PAUSE:
                        game.request_pause()
                    elif not game.ENABLE_SLIDING:
                        if event.key == Game.RIGHT:
                            game.request_right()
                        elif event.key == Game.LEFT:
                            game.request_left()
                pass
            key_pressed = pygame.key.get_pressed()
            now = pygame.time.get_ticks()
            if key_pressed[Game.DOWN]:
                game.request_softdrop()
            elif key_pressed[Game.EXIT2]:
                game.is_running = False
            elif game.ENABLE_SLIDING and now - last_keypress > key_delay:
                if key_pressed[Game.RIGHT]:
                    game.request_right()
                    last_keypress = now
                elif key_pressed[Game.LEFT]:
                    game.request_left()
                    last_keypress = now
        else:
            # process user input - postgame
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    game.is_running = False
                pass
                key_pressed = pygame.key.get_pressed()
                if key_pressed[Game.EXIT1]:
                    game.is_running = False
                elif key_pressed[Game.EXIT2]:
                    game.is_running = False
                elif key_pressed[Game.RESTART]:
                    gdim = (game.well.w, game.well.h_actual)
                    uidim = (ui.w, ui.h)
                    game = Game(gdim, Ui.SCALE, screen, COLORS, is_paused=False)
                    ui = Ui(uidim, Ui.SCALE, screen, COLORS, game)
        
        # update pygame
        clock.tick(Game.FPS)
        pygame.display.flip()
    pygame.quit()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Yet Another Tetris Clone.")
    parser.add_argument("--scaling", action="store", dest="SCALE", type=int,
                        help="window scaling factor")
    parser.add_argument("--width", action="store", dest="WELL_W", type=int,
                        help="well width")
    parser.add_argument("--height", action="store", dest="WELL_H", type=int,
                        help="well height")
    parser.add_argument("--level", action="store", dest="INIT_LEVEL", type=int,
                        help="initial level")
    parser.add_argument("--fancy", action="store_true", dest="FANCY_COL", 
                        help="use fancy colors")
    parser.add_argument("--sliding", action="store_true", dest="ENABLE_SLIDING",
                        help="enable sliding sideways moves")
    
    results = parser.parse_args()
    if results.SCALE is not None:
        Ui.SCALE = max(results.SCALE, 1)
        Ui.TEXT_BIG = Ui.SCALE * 2
        Ui.TEXT_SMALL = Ui.SCALE
    if results.WELL_W is not None:
        Well.W = max(results.WELL_W, 4)
    if results.WELL_H is not None:
        Well.H = max(results.WELL_H, 1)
    if results.INIT_LEVEL is not None:
        Game.INIT_LEVEL = max(results.INIT_LEVEL, 0)
    if results.FANCY_COL is True:
        Ui.FANCY_COL = True
    if results.ENABLE_SLIDING:
        Game.ENABLE_SLIDING = True
    
    main()
